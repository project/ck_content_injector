<?php
/**
 * @file
 * Definition of PluginParser
 */

class PluginParser {

  /**
   * Display html for dialog form parameters.
   *
   * @param string $plugin
   *   name of template file
   */
  public function displayForm($plugin) {
    $content_creation_links = array();
    $fields = $this->parseValues($plugin, $content_creation_links);
    $tidy_ups = array();
    $disabled_fields = array();
    print '<script type="text/javascript">tidyUps={};</script>' . "\n";
    print '<div class="cke_dialog_ui_vbox cke_dialog_page_contents">' . "\n";
    // Output the content creation links from the template, if we have any.
    if (is_array($content_creation_links) && count($content_creation_links) > 0) {
      print '  <fieldset>' . "\n";
      print '    <legend>&nbsp;Create new content&nbsp;</legend>' . "\n";
      print '    <ul class="injector_content_creation_links">' . "\n";
      foreach ($content_creation_links as $link) {
        print '      <li>' . $link . '</li>';
      }
      print '    </ul>' . "\n";
      print '  </fieldset>' . "\n";
    }
    // Output the form fields from the template.
    foreach ($fields as $settings) {
      print '<div class="cke_dialog_ui_vbox_child" role="presentation"' . (!empty($settings['disabled']) ? ' style="display: none;"' : '') . '>';
      switch ($settings['type']) {
        case 'dropdown':
        case 'select':
          print '<div class="cke_dialog_ui_select" role="presentation">';
          print '<label>' . htmlspecialchars($settings['label']) . '</label>';
          print '<div class="cke_dialog_ui_select" role="presentation">';
          print '<select class="cke_dialog_ui_input_select" name="field[]" id="t_field_' . $settings['index'] . '" order="' . $settings['index'] . '">';
          foreach ($settings['options'] as $key => $val) {
            print '<option value="' . htmlentities($key) . '">' . htmlspecialchars($val) . '</option>';
          }
          print '</select>';
          print '</div>';
          print '</div>';
          break;

        case 'textbox':
        default:
          $autocomplete_attr = (!empty($settings['autocomplete_url'])) ? 'data-ac-url="' . $settings['autocomplete_url'] . '"' : '';
          $autocomplete_class = (!empty($settings['autocomplete'])) ? ' cke_autocomplete' : '';
          // Field attributes.
          $size = (!empty($settings['size']) && is_numeric($settings['size'])) ? (int) $settings['size'] : 40;
          $required = (!empty($settings['required']) && $settings['required']) ? 1 : 0;
          $regexp = (!empty($settings['regex']['pattern'])) ? $settings['regex']['pattern'] : '';
          $regexm = (!empty($settings['regex']['modifier'])) ? $settings['regex']['modifier'] : '';
          $ac_regexp = (!empty($settings['autocomplete_replace']['pattern'])) ? $settings['autocomplete_replace']['pattern'] : '';
          $ac_regexm = (!empty($settings['autocomplete_replace']['modifier'])) ? $settings['autocomplete_replace']['modifier'] : '';
          $ac_node_types = '';
          if (!empty($settings['autocomplete_node_types'])) {
            $ac_node_types = is_array($settings['autocomplete_node_types']) ? implode(' ', $settings['autocomplete_node_types']) : $settings['autocomplete_node_types'];
          }
          print '<div class="cke_dialog_ui_text" role="presentation">';
          print '<label>' . htmlspecialchars($settings['label']) . '</label>';
          print '<div class="cke_dialog_ui_input_text ui-widget" role="presentation">';
          print '<input class="cke_dialog_ui_input_text' . $autocomplete_class . '" ' . $autocomplete_attr . ' type="text" name="field[]" id="t_field_' . $settings['index'] . '" order="' . $settings['index'] . '" value="" label="' . $settings['label'] . '" size="' . $size . '" required="' . $required . '" regexp="' . $regexp . '" regexm="' . $regexm . '" data-ac-regexp="' . $ac_regexp . '" data-ac-regexm="' . $ac_regexm . '" data-ac-node-types="' . $ac_node_types . '" />';
          print '</div>';
          print '</div>';
          break;
      }
      print '</div>';
      if (!empty($settings['tidy-ups']) && is_array($settings['tidy-ups'])) {
        $tidy_ups[$settings['index']] = $settings['tidy-ups'];
      }
    }
    print '</div>' . "\n";
    print '<script type="text/javascript">tidyUps=' . json_encode($tidy_ups) . '; disabledFields=' . json_encode($disabled_fields) . ';</script>' . "\n";
    print '<script type="text/javascript">CK_Content_Injector.bindFieldEvents();</script>';
  }

  /**
   * Get parameter values from template file.
   *
   * @param string $plugin
   *   Name of template file, including full path.
   *
   * @param array $links
   *   Output array for storing any content creation links to be rendered in the template alongside the input form. Pass in a blank array to have it populated with renderable Drupal links as provided by the template.
   *
   * @return array
   *   Template fields
   */
  protected function parseValues($plugin, &$links = array()) {
    $get_form_elements = TRUE;
    ob_start();
    require($plugin);
    ob_end_clean();
    $fields = array();
    // Set weight ordering and field position.
    if (!empty($params) && is_array($params)) {
      $tmp = array();
      $index = 0;
      foreach ($params as $label => $field) {
        // Get field weight.
        $weight = (!empty($field['weight'])) ? $field['weight'] : 0;
        // Ensure temporary weight array exists.
        if (empty($tmp[$weight])) {
          $tmp[$weight] = array();
        }
        // Add additional field values.
        $field['label'] = $label;
        $field['index'] = $index;
        // Build temporary weight orders array.
        $tmp[$weight][] = $field;
        // Increment field index counter, but only if this is not a markup field (markup fields don't get a data value).
        if ($field['type'] !== 'markup') {
          $index++;
        }
      }
      // Order tmp array by weight key.
      ksort($tmp);
      // Build field array ordered by weight.
      foreach ($tmp as $w_fields) {
        foreach ($w_fields as $f) {
          $fields[] = $f;
        }
      }
    }
    // Get the content creation links from the template.
    if (!empty($content_creation_links) && is_array($content_creation_links)) {
      foreach ($content_creation_links as $link) {
        $links[] = $link;
      }
    }
    return $fields;
  }
}
