CONTENTS OF THIS FILE
---------------------

 * Overview
 * Installation
 * How To
 * Form Definition

Overview
--------
This module enables you to build your own custom content injector that
integrates with CK Editor. It requires the ckeditor module to be installed and
enabled on your environment.

Installation
------------
Installing the module is simple:

1) Copy the ck_content_injector folder to the modules folder in your
   installation.
2) Enable the module using Administer -> Modules (/admin/build/modules)
3) Optional: configure permissions. By default, users need the 'administer
   nodes' permission to configure and use the content injector. Additionally,
   you can grant users 'use ck injector' and 'administer ck injector'
   permissions to fine-tune access.

How To
------
The following is a brief description of how to add custom injector templates:

1) If you have a folder called 'ck_injector_templates' in your theme (or in
   your libraries folder), any .php file that is placed in it will be made
   available as an injector template in the CK Editor.
2) In your template file, the $params array is used to define the form fields
   for the CK Editor dialog box. See the next section for details on how to
   define them.
3) The $parameter_values array will contain the values entered by the end-user
   for the template in the same order as they were defined in $params.
     - this array will be used when outputting the content
     - see the contents of the 'example_injector_templates' folder (e.g.
       node.php) for example injector templates

Form Definition
---------------
Details on how to implement the form field definition array "$params":

Format:
$params=array(
  'field label' => array(
    'type' => 'field-type',
    'field_name' => 'my_field', *
    'regex' => array(
      'pattern' => '^[a-z]$', -> REGEX pattern, exclude leading and trailing /
      'modifiers' => 'i', -> REGEX modifiers
    ),
    'options' => array('value' => 'text', 'value' => 'text'), **
    'weight' => 1, -> used to order fields within the dialog form, default = 0
    'required' => TRUE/FALSE, -> defined the field as required
  )
);

Field-types:
  textbox - a simple input field
  dropdown - a select box field

* optional, if defined will be available as an element of the $parameter_values 
  array, e.g - $parameter_values['my_field']
** only needed for dropdown field-type
