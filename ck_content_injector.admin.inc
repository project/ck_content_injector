<?php
/**
 * @file
 * Admin page/functions to manage ck content injector settings.
 */

/**
 * Form constructor for admin form.
 *
 * @see ck_content_injector_admin_form_submit()
 */
function ck_content_injector_admin_form($state) {
  $form = array();

  $form['msg'] = array(
    '#type' => 'item',
    '#value' => check_plain('Manage fields to apply CK Content Injector token replacement to, this applies to the BODY by default.'),
  );

  // Get all node types.
  $types = node_type_get_types();

  // Loop types and get all text area (multi row) fields for the node type.
  foreach ($types as $t) {
    $form[$t->type] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => check_plain('Content type: ' . $t->name),
      '#description' => check_plain('Select the fields CK Content Injector tokens should be processed for.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    // Get fields for node type.
    $fields = array();
    $fields = add_fields($t->type, $fields);

    if (count($fields) > 0) {
      $form[$t->type]['fields'] = array(
        '#type' => 'checkboxes',
        '#title' => check_plain('Fields'),
        '#options' => $fields,
        '#default_value' => variable_get('ck_content_injector_' . $t->type . '_fields', array()),
      );
    } else {
      $form[$t->type]['fields'] = array(
        '#markup' => '<p>' . check_plain('No compatible text fields defined that can be used with CK Content Injector.') . '</p>',
      );
    }
  }

  // Field Collections.
  $fc_sql = 'SELECT i.* FROM {field_config_instance} AS i JOIN {field_config} AS c ON i.field_id = c.id WHERE c.module = :field';
  $fc_result = db_query($fc_sql, array(':field' => 'field_collection'));
  while ($fc_r = $fc_result->fetchObject()) {
    $fc_f_sql = 'SELECT i.* FROM {field_config_instance} AS i JOIN {field_config} AS c ON i.field_id = c.id WHERE c.module = :field AND i.bundle = :type';
    $fc_f_result = db_query($fc_f_sql, array(':field' => 'text', ':type' => $fc_r->field_name));
    $fc_data = unserialize($fc_r->data);
    $fields = array();
    while ($fc_f_r = $fc_f_result->fetchObject()) {
      $data = unserialize($fc_f_r->data);
      $fields[$fc_f_r->field_name] = $data['label'];
    }
    $form[$fc_r->field_name] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => check_plain('Field collection: ' . $fc_data['label']),
      '#description' => check_plain('Select the fields CK Content Injector tokens should be processed for.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    if (count($fields) > 0) {
      $form[$fc_r->field_name]['fields'] = array(
        '#type' => 'checkboxes',
        '#title' => check_plain('Fields'),
        '#options' => $fields,
        '#default_value' => variable_get('ck_content_injector_' . $fc_r->field_name . '_fields', array()),
      );
    } else {
      $form[$fc_r->field_name]['fields'] = array(
        '#markup' => '<p>' . check_plain('No compatible text fields defined that can be used with CK Content Injector.') . '</p>',
      );
    }
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

  return $form;
}

function add_fields($type, $fields, $prefix = '', $collection = '') {
  // Load the fields on this node.
  $sql = 'SELECT i.* FROM {field_config_instance} AS i JOIN {field_config} AS c ON i.field_id = c.id WHERE c.module = :field AND i.bundle = :type';
  $result = db_query($sql, array(':field' => 'text', ':type' => $type));
  while ($r = $result->fetchObject()) {
    $data = unserialize($r->data);
    $fields[$collection . $r->field_name] = $prefix . $data['label'];
  }
  // Load the field collections for this content type.
  $sql2 = 'SELECT i.* FROM {field_config_instance} AS i JOIN {field_config} AS c ON i.field_id = c.id WHERE c.module = :field AND i.bundle = :type';
  $result2 = db_query($sql2, array(':field' => 'field_collection', ':type' => $type));
  while ($r2 = $result2->fetchObject()) {
    $data2 = unserialize($r2->data);
    $fields = add_fields($r2->field_name, $fields, $prefix . $data2['label'] .' -> ', $collection . $r2->field_name . ':');
  }
  return $fields;
}

/**
 * Form submission for ck_content_injector_admin_form()
 */
function ck_content_injector_admin_form_submit($form, $state) {
  // Get all node types.
  $types = node_type_get_types();

  // Set variables for node type field settings.
  foreach ($types as $t) {
    if (!empty($state['values'][$t->type]['fields'])) {
      variable_set('ck_content_injector_' . $t->type . '_fields', $state['values'][$t->type]['fields']);
    }
    else {
      variable_del('ck_content_injector_' . $t->type . '_fields');
    }
  }
  $fc_sql = 'SELECT i.* FROM {field_config_instance} AS i JOIN {field_config} AS c ON i.field_id = c.id WHERE c.module = :field';
  $fc_result = db_query($fc_sql, array(':field' => 'field_collection'));
  while ($fc_r = $fc_result->fetchObject()) {
    if (!empty($state['values'][$fc_r->field_name]['fields'])) {
      variable_set('ck_content_injector_' . $fc_r->field_name . '_fields', $state['values'][$fc_r->field_name]['fields']);
    } else {
      variable_del('ck_content_injector_' . $fc_r->field_name . '_fields');
    }
  }
}

/**
 * Form constructor for settings form.
 *
 * @see ck_content_injector_settings_form_submit()
 */
function ck_content_injector_settings_form($state) {
  $form = array();
  // Settings for node types available for autocomplete lookup.
  $form['autocomplete'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#title' => 'Node autocomplete settings',
    '#description' => check_plain('Select which node types will be lookup by the autocomplete field type, if none selected them all node types will be included.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Get all node types.
  $types = node_type_get_types();
  $options = array();
  foreach ($types as $t) {
    $options[$t->type] = $t->name;
  }
  $form['autocomplete']['autocomplete_types'] = array(
    '#type' => 'checkboxes',
    '#title' => check_plain('Node types'),
    '#options' => $options,
    '#default_value' => variable_get('ck_content_injector_autocomplete_types', array()),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save settings',
  );
  return $form;
}

/**
 * Form submission for ck_content_injector_settings_form()
 */
function ck_content_injector_settings_form_submit($form, $state) {
  if (is_array($state['values'])) {
    foreach ($state['values'] as $var => $val) {
      variable_set('ck_content_injector_' . $var, $val);
    }
  }
}
