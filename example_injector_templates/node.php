<?php
/**
 * @file
 * Node injector template.
 */

// Dialog box parameters.
$params = array(
  'Node ID' => array(
    'type' => 'textbox',
    'regex' => array(
      'pattern' => '^[0-9]+$',
      'modifier' => '',
    ),
    'field_name' => 'node_id',
    'autocomplete' => TRUE,
    'autocomplete_replace' => array(
      'pattern' => '(.*NID:)[^0-9]',
      'modifier' => '',
    ),
    'weight' => 1,
    'required' => TRUE,
  ),
  'Width (px)' => array(
    'type' => 'textbox',
    'regex' => array(
      'pattern' => '^[0-9]+$',
      'modifier' => '',
    ),
    'field_name' => 'width',
    'weight' => 2,
  ),
  'Height (px)' => array(
    'type' => 'textbox',
    'regex' => array(
      'pattern' => '^[0-9]+$',
      'modifier' => '',
    ),
    'field_name' => 'height',
    'weight' => 3,
  ),
);

// Helpful relevant content creation links for outputting to the user.
$content_creation_links = array(
  l('Create a new Article node', 'node/add/article', array('attributes' => array('target' => '_blank'))),
);

if (!empty($parameter_values)) {
  $styles = '';
  if (!empty($parameter_values['width'])) {
    $styles .= 'width:' . (int) $parameter_values['width'] . 'px;';
  }
  if (!empty($parameter_values['height'])) {
    $styles .= 'height:' . (int) $parameter_values['height'] . 'px;';
  }
  $node = node_load($parameter_values['node_id']);
  ?>
  <div style="<?php print $styles; ?>">
    <h2><?php print $node->title; ?></h2>
    <?php print $node->body['und'][0]['safe_value']; ?>
  </div>
  <?php
}
