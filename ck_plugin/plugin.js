/*
 * @file
 * CK Content Injector Plugin.
 * Plugin to inject a template in the editor.
 *
 */

/**
 * Register the CK Content Injector plugin with CKEditor.
 */
CKEDITOR.plugins.add( 'ck_content_injector',
{
  init: function( editor )
  {
    /**
     * Register the 'command' we are creating with the CKEditor.
     */
    editor.addCommand( 'LoadCKContentInjector', new CKEDITOR.dialogCommand( 'LoadCKContentInjector' ) );

    /**
     * Create the toolbar button.
     */
    editor.ui.addButton( 'ck_content_injector',
    {
      label: Drupal.t('Embed'),
      command: 'LoadCKContentInjector',
      icon: this.path + 'toolbaricon.png'
    });
    
    /**
     * Create the dialog box associated with this plugin.
     */
    CKEDITOR.dialog.add( 'LoadCKContentInjector', this.path + 'dialogs/ck_content_injector.js' );

    /**
     * Detect double-click events. We are particularly interested in the user
     * double-clicking on a "TEMPLATE" <SPAN> tag, where we should counteract
     * any attempts by the browser to select other elements as well.
     */
    editor.on( 'doubleclick', function(event) {
      if (editor.readOnly)
        return;
      // Get the element that was clicked on.
      var targetElement = event.data.path.lastElement;
      // Test if the clicked element is a "TEMPLATE" span.
      if ( targetElement && targetElement.is('span') ) {
        if( targetElement.getText().substr(0, 10) == '[[TEMPLATE') {
          // Trim the selection down to just our element.
          editor.getSelection().selectElement(targetElement);
          //editor.openDialog( 'LoadCKContentInjector' );
        }
      }
    });

    /**
     * Detect when the user changes the active selection in the editor area. We
     * are particularly interested in when they click on "TEMPLATE" <SPAN> tags,
     * so we save a reference to the currently active one in a variable.
     */
    editor.on( 'selectionChange', function(event) {
      if (editor.readOnly)
        return;
      // Get the element that was selected.
      var selectedElement = event.data.path.lastElement;
      // We're only interested in selection changes to our "TEMPLATE" span tags.
      if ( selectedElement && selectedElement.is('span') && selectedElement.getText().substr(0, 10) == '[[TEMPLATE') {
        // Store the clicked element in our object.
        CK_Content_Injector.selectedTemplateTag = selectedElement;
      }
      else {
        // Reset the clicked element reference.
        CK_Content_Injector.selectedTemplateTag = null;
      }
    });

    /**
     * Highlight existing injector template tags in this document.
     */
    editor.on( 'instanceReady', function(event) {
      CK_Content_Injector.FindTemplateTags(editor);
    });
  }
});

/**
 * The object used for all operations.
 */
var CK_Content_Injector = {
  editor : null,
  templateSelectorId : null,
  selectedTemplateTag : null,
  injectorTemplates : []
};

/**
 * Loads in a list of the available templates. This function uses the Drupal
 * module's "get_template_list" function to get them.
 */
CK_Content_Injector.loadTemplateList = function() {
  jQuery.get(Drupal.settings.basePath + 'admin/ck_content_injector/get_template_list', {}, function(data) {
    CK_Content_Injector.injectorTemplates = [];
    CK_Content_Injector.injectorTemplates = data;
  }, 'json');
}

/**
 * Fills the template selection drop-down box with either a list of available
 * templates (if no template tag is currently selected), or the filename of the
 * current tag (if one is).
 */
CK_Content_Injector.populateTemplatesDropdown = function(selectedTemplate) {
  // Get a reference to the template selection drop-down box.
  var dd = document.getElementById(CK_Content_Injector.templateSelectorId),
      templates = Object.keys(CK_Content_Injector.injectorTemplates),
      template = templates.length === 1 ? templates[0] : '';
  // Empty the drop-down.
  dd.options.length = 0;
  // If a "TEMPLATE" tag was highlighted in the editor, add its type as the only available option.
  if (''!=selectedTemplate) {
    var newOption = document.createElement('OPTION');
    newOption.text = CK_Content_Injector.injectorTemplates[selectedTemplate];
    newOption.value = selectedTemplate;
    newOption.selected = true;
    dd.options.add(newOption);
  }
  // Otherwise, no "TEMPLATE" tag was highlighted so present a blank form.
  else {
    // Empty the form (or load the single chosen template).
    CK_Content_Injector.templateSelectionChanged(template);
    // Add the 'Please select...' option first to the drop-down.
    var pleaseSelectOption = document.createElement('OPTION');
    pleaseSelectOption.text = Drupal.t('Please select...');
    pleaseSelectOption.value = '';
    pleaseSelectOption.selected = true;
    dd.options.add(pleaseSelectOption);
    // Now add each available injector template as a separate option.
    for (var templateName in CK_Content_Injector.injectorTemplates) {
      var newOption = document.createElement('OPTION');
      newOption.text = CK_Content_Injector.injectorTemplates[templateName];
      newOption.value = templateName;
      dd.options.add(newOption);
    }
  }
  // If there's only one template to choose from, hide the template selection
  // box.
  if (templates.length === 1) {
    jQuery(dd).closest('tr').css('display', 'none');
  }
}

/**
 * Fills the fields in the template form with the variables from the currently-
 * selected "TEMPLATE" tag.
 */
CK_Content_Injector.populateTemplateForm = function(templateVars) {
  // Break the template variables string into its components.
  var aParts = Array.prototype.slice.call(templateVars);
  // Attempt to get a reference to a field corresponding to the current component.
  for (var i = 1; i < aParts.length; i++) {
    var field = document.getElementById('t_field_'+(i-1));
    // If the field exists, attempt to set its value.
    if (field != undefined) {
      var value = unescape(aParts[i]);
      if( tidyUps[i-1]) {
        for (var j in tidyUps[i-1]) {
          if (value == j) {
            value = tidyUps[i-1][j];
            break;
          }
        }
      }
      field.value = value;
    }
  }
};

/**
 * Triggered when the template selection drop-down box is changed. Fills the
 * form DIV with the appropriate form fields for the chosen template.
 */
CK_Content_Injector.templateSelectionChanged = function(template, callback, aParts) {
  if (!template) {
    jQuery('#formContents').empty();
    return;
  }
  if (callback && 'function'==typeof callback && aParts) {
    jQuery('#formContents').load(Drupal.settings.basePath + 'admin/ck_content_injector/load_form_content', { plugin: template }, function() { callback.apply(window, aParts) });
  }
  else {
    jQuery('#formContents').load(Drupal.settings.basePath + 'admin/ck_content_injector/load_form_content', { plugin: template });
  }
};

/**
 * Adds a new "TEMPLATE" <SPAN> tag at the current location (selection) in the
 * editor, giving it the parameters sent to this function.
 */
CK_Content_Injector.Add = function() {
  // Create the new <SPAN> tag that we will be inserting into the document.
  var oSpan = new CKEDITOR.dom.element('SPAN');
  // Break apart the arguments sent to this function.
  var args = Array.prototype.slice.call(arguments);
  // Add our new span tag to the new array of arguments.
  args.unshift(oSpan);
  // Call a helper function that customises this <SPAN> tag.
  this.SetupSpan.apply(this, args);
  // Insert the new <SPAN> tag into the document.
  CK_Content_Injector.editor.insertElement(oSpan);
}

/**
 * Helper function for configuring a new "TEMPLATE" <SPAN> document element with
 * the correct parameters.
 */
CK_Content_Injector.SetupSpan = function() {
  var args = Array.prototype.slice.call(arguments);
  var span = args.shift();
  // Customise the look and feel of the <SPAN> tag.
  span.setHtml('[[TEMPLATE ' + args.join(',') + ' ]]');
  this.StyleSpan(span);
}

/**
 * Helper function to apply styling to a "TEMPLATE" <SPAN> document element.
 */
CK_Content_Injector.StyleSpan = function(spanElement) {
  if(spanElement) {
    spanElement.setStyles({
      'background-color' : '#ccffcc',
      'color' : '#000000'
    });
    spanElement.setAttributes({
      'contenteditable' : 'false'
    });
    if (CKEDITOR.env.gecko) {
      spanElement.setStyle('cursor', 'default');
    }
  }
  else {
    return 'style="background-color: #ccffcc; color: #000000; cursor: default;" contenteditable="false"';
  }
}

/**
 * Search the given editor instance for content injector TEMPLATE tags and wrap
 * them in <SPAN> tags.
 */
CK_Content_Injector.FindTemplateTags = function(editor) {
  if(!editor) {
    if(CK_Content_Injector.editor) {
      editor = CK_Content_Injector.editor;
    }
    else {
      return;
    }
  }
  var body = editor.document.getBody().$;
  // Ensure all TEMPLATE tag instances are wrapped in a contenteditable=false span.
  body.innerHTML = body.innerHTML.replace(/(<span((?!>).)*>\[\[TEMPLATE)([^\]]*?)(\]\]<\/span>)/g, '<span ' + CK_Content_Injector.StyleSpan() + '>[[TEMPLATE$3]]</span>');
  body.innerHTML = body.innerHTML.replace(/(<p((?!>).)*>\[\[TEMPLATE)([^\]]*?)(\]\]<\/p>)/g, '<p><span ' + CK_Content_Injector.StyleSpan() + '>[[TEMPLATE$3]]</span></p>');
}

CK_Content_Injector.bindFieldEvents = function() {
  jQuery('input.cke_autocomplete').each(function(){
    var element = jQuery(this);
    // Build autocomplete url.
    var uri = 'admin/ck_content_injector/node/autocomplete';
    var protocol = window.location.protocol;
    var host = window.location.host;
    var base = Drupal.settings.basePath;
    var types = [];
    if (element.attr('data-ac-node-types') != undefined) {
      types = element.attr('data-ac-node-types').split(' ');
    }
    var types_str = '';
    for (var i = 0; i < types.length; i++) {
      types_str += (i === 0) ? '?' : '&';
      types_str += 'types[]=' + types[i];
    }
    var url = protocol + '//' + host + base + uri + types_str;
    // Check for overwrite attribute.
    if (element.attr('data-ac-url') != undefined) {
      var url = element.attr('data-ac-url');
    }
    // Attach autocomplete handler.
    element.autocomplete({
      source: url,
      select: function(event, ui){
        // Check for no results message.
        if (ui.item.value == 'No results match your term') {
          ui.item.value = '';
        }
        else{
          var value = ui.item.value;
          var regexp = element.attr('data-ac-regexp');
          var regexm = element.attr('data-ac-regexm');
          if (regexp != '' && value != '' && regexp != undefined) {
            var regexObj = new RegExp(regexp, regexm);
            var value = value.replace(regexObj, '');
          }
          ui.item.value = value;
        }
      }
    });
  });
}

/**
 * Load a list of the available templates when the page is ready.
 */
jQuery(function(){
  CK_Content_Injector.loadTemplateList();
});
