/*
 * @file
 * CK Content Injector Plugin Main Dialog.
 * Definition of the dialog box for the CK Content Injector plugin.
 *
 */

CKEDITOR.dialog.add( 'LoadCKContentInjector', function( editor )
{
  return {
    title: Drupal.t('Embed'),
    minWidth: 400,
    minHeight: 300,
    contents: [{
      id: 'ck_content_injector_tab_1',
      label: '',
      elements: 
      [
        // Create a selection box that will be populated with the available templates.
        {
          type: 'select',
          id: 'cmbInjectorTemplate',
          onLoad: function() {
            // Store the ID of this form element for reference.
            CK_Content_Injector.templateSelectorId = this.getInputElement().getAttribute('id');
            // Add a listener function to detect when this selection box is changed.
            jQuery('#'+CK_Content_Injector.templateSelectorId).change(function(){
              var template = jQuery(this).val();
              CK_Content_Injector.templateSelectionChanged(template);
            });
          },
          label: Drupal.t('Select template'),
          labelLayout: 'vertical',
          items: [[Drupal.t('Please select...'), 0]],
          default: 0
        },
        // Add an empty <DIV> that we can populate with our form contents later.
        {
          type: 'html',
          html: '<div id="formContents" style="overflow-y: auto; height: 260px;"></div>'
        }
      ],
    }],
    onShow: function() {
      // Store a reference to the active CKEditor instance.
      CK_Content_Injector.editor = editor;
      // See if a "TEMPLATE" <SPAN> tag was selected when this dialog was opened.
      if (CK_Content_Injector.selectedTemplateTag) {
        // Grab the HTML content of the tag.
        var targetContent = CK_Content_Injector.selectedTemplateTag.getHtml().match( /\[\[TEMPLATE\s*([^\]]*?)\s*\]\]/ )[1];
        // Break up the content's comma-separated values.
        var aParts = targetContent.split(",");
        // Get the filename of the template file.
        aParts[0] = aParts[0].replace(/\.php$/, '') + '.php';

        CK_Content_Injector.templateSelectionChanged(aParts[0], function() {
          CK_Content_Injector.populateTemplateForm(arguments);
          CK_Content_Injector.populateTemplatesDropdown(aParts[0]);
        }, aParts);

        // Ensure that we have fully selected the existing tag so that the following function correctly replaces it.
        editor = CK_Content_Injector.editor;
        editor.getSelection().selectElement(CK_Content_Injector.selectedTemplateTag);
      }
      else {
        CK_Content_Injector.populateTemplatesDropdown('');
      }
    },
    onOk: function() {
      var errorStr = "";
      var templateSelector = document.getElementById(CK_Content_Injector.templateSelectorId);
      var templates = Object.values(CK_Content_Injector.injectorTemplates);
      var selectedTemplate = '';
      if (templates.length === 1) {
        selectedTemplate = templates[0];
      }
      else {
        if (templateSelector.value.length == 0 || templateSelector.value == 0) {
          errorStr = "\n" + Drupal.t('A template has not been selected');
        }
        else {
          selectedTemplate = document.getElementById(CK_Content_Injector.templateSelectorId).value;
        }
      }
      // Compile the values of the form fields into an array.
      var args = [selectedTemplate.replace(/\.php$/, '')];
      var fields = document.getElementsByName('field[]');
      for (var i = 0; i < fields.length; i++) {
        var order = parseInt(fields[i].getAttribute('order')) + 1;
        var regexp = fields[i].getAttribute('regexp');
        var regexm = fields[i].getAttribute('regexm');
        if (parseInt(fields[i].getAttribute('required')) == 1 && fields[i].value == '') {
          errorStr += "\n" + fields[i].getAttribute('label') + ' is required!';
        }else if (regexp != '' && fields[i].value != '' && regexp != null) {
          var regexObj = new RegExp(regexp, regexm);
          if (!fields[i].value.match(regexObj)) {
            errorStr += "\n" + 'Invalid value entered for ' + fields[i].getAttribute('label');
          }
        }
        args[order] = escape(fields[i].value);
      }
      if (errorStr.length > 0) {
        errorStr = "The following error(s) have occurred: " + errorStr;
        alert(errorStr);
        return false;
      }
      // Pass the form values to the CK_Content_Injector.Add() function to insert the "TEMPLATE" tag into the document.
      CK_Content_Injector.Add.apply(CK_Content_Injector, args);
      // Reset the CK_Content_Injector object's tracking variables.
      CK_Content_Injector.editor = null;
      CK_Content_Injector.selectedTemplateTag = null;
    },
  };
});
